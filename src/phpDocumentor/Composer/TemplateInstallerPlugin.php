<?php

namespace phpDocumentor\Composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class TemplateInstallerPlugin implements PluginInterface
{
	public function activate(Composer $composer, IOInterface $io)
	{
		//$io->ask('IOInterface se pta');  
		//$io->write('IOInterface add installator');
		$installer = new TemplateInstaller($io, $composer);
		echo "\n [PACKAGE - ADD INSTALLER] \n";
		$composer->getInstallationManager()->addInstaller($installer);
	}
}