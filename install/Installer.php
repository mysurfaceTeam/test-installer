<?php

use CMS\Installer\BasePackageInstaller;
use Composer\IO\IOInterface;
use Composer\Installer\PackageEvent;
use Composer\Package\CompletePackage;
use Composer\Package\PackageInterface;

class PackageInstaller  extends BasePackageInstaller {

	public function install(PackageInterface $package, $force = FALSE)
	{
		$packageName = $package->getName();

		if (!$force) {
			if (!$this->ask('Do you want install package ' . $packageName . "?")) {
				return;
			}
		}

		$this->io->write('START INSTALLATION .....');
		// @todo start installation
		$this->io->write('DONE INSTALLATION .....');
	}

	/**
	 * @param PackageInterface $package
	 * @param bool             $force
	 */
	public function update(PackageInterface $package, $force = FALSE)
	{
		$packageName = $package->getName();
		if (!$force) {
			if (!$this->ask('Do you want reinstall package ' . $packageName . "?")) {
				return;
			}
		}

		$this->unInstall($package, TRUE);
		$this->install($package, TRUE);
	}

	/**
	 * @param PackageInterface $package
	 * @param bool             $force
	 */
	public function unInstall(PackageInterface $package, $force = FALSE)
	{
		$packageName = $package->getName();
		if (!$force) {
			if (!$this->ask('Do you want uninstall package ' . $packageName . "?") && !$force) {
				return;
			}
		}

		$this->io->write('START UNINSTALL');
		// @todo uninstall
		$this->io->write('DONE UNINSTALL');
	}

	/**
	 * @param PackageInterface $package
	 * @return bool
	 */
	public function isInstalled(PackageInterface $package)
	{
		return FALSE;
	}

}
